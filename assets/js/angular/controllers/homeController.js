/**
 * Created by thickam on 11/16/15.
 */
var app = angular.module('pizzaApp'); //retrieves the module without creating a new one

app.controller('homeController', function($scope, $http, dbService) {
  $scope.editItem = {};
  $scope.items = [];
  dbService.getAll("item").then(function(response) {
    $.each(response.data, function(index, element) {
      $scope.items.push(element);
    });
  });
  $scope.create = function() {
    dbService.create("item", $scope.editItem).then(function(response) {
        $scope.items.push(response.data);
        $scope.editItem.name = null;
      });
  };
  $scope.destroy = function(id, index) {
    dbService.destroy("item", id).then(function(response) {
      $scope.items.splice(index, 1);
    });
  };
  $scope.update = function(id, index) {
    dbService.update("item", id, $scope.items[index]).then(function(response) {
      $scope.items.splice(index, 1, response.data);
    });
  };
});
