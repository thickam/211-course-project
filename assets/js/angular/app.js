/**
 * Created by thickam on 11/15/15.
 */
var app = angular.module('pizzaApp', []); //creates the module

app.factory('dbService', function($http) {
  var dbService = {};
  dbService.getAll= function(modelName) {
    return $http.get("/" + modelName);
  };
  dbService.getById = function(modelName, modelId) {
    return $http.get("/" + modelName + "/" + modelId);
  };
  dbService.create = function(modelName, model) {
    return $http.post("/" + modelName + "/create/", model);
  };
  dbService.destroy = function(modelName, modelId) {
    return $http.get("/" + modelName + "/destroy/" + modelId);
  };
  dbService.update = function(modelName, modelId, model) {
    return $http.post("/" + modelName + "/update/" + modelId + "?", model);
  };
    return dbService;
});
